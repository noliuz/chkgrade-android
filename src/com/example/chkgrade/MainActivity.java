package com.example.chkgrade;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends Activity {

	Button chk_butt;
	EditText id_et;
	EditText ip_et;	
	TextView name_tv;
	String get_msg;
	TableLayout tl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		
		chk_butt = (Button)findViewById(R.id.button1);
		id_et = (EditText)findViewById(R.id.editText1);
		ip_et = (EditText)findViewById(R.id.editText2);
		name_tv = (TextView)findViewById(R.id.textView1);
		tl = (TableLayout)findViewById(R.id.tl1);
		
		chk_butt.setOnClickListener(new OnClickListener()
	    {
	      public void onClick(View v)
	      {
	          //check if empty
	    	  if (ip_et.getText().toString().trim().isEmpty() || 
	             id_et.getText().toString().trim().isEmpty())
	        	 return;
	         
	    	  //call web
	    	  String url = "http://"+ip_et.getText().toString().trim()+"/chkgrade/?code="+
	    			  id_et.getText().toString().trim();	    	  	
	    	  //url = "http://www.google.co.th";
	    	  Log.e("http_prepare",url);
	    	  
	    	  
	    	  try {
    	        HttpClient httpclient = new DefaultHttpClient();
    	        URI uri = new URI(url);    	        
    	        HttpGet httpget = new HttpGet();
    	        httpget.setURI(uri);	    	    
    	        
	    	    BufferedReader in = null;
	    	    HttpResponse response;
	    	    
	    	    response = httpclient.execute(httpget);    	        
	    	    response.getStatusLine().getStatusCode();
	    	    
    	        StringBuffer sb = new StringBuffer("");
    	        in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    	        
    	        String l = "";
                String nl = System.getProperty("line.separator");
                while ((l = in.readLine()) !=null){
                    sb.append(l + nl);
                }
                in.close();
                get_msg = sb.toString();         	       
	    	  
                if (get_msg.isEmpty())
                	return;
                
                updateTable();
                
                Log.e("return",get_msg);
	    	  } catch (Exception e) {
	    		e.printStackTrace();
	    	  }	    	  
	      }
	    });
		
		
	}

	public void updateTable() {
		
		tl.removeAllViews();
		
		String[] sp = get_msg.split("<br>");
		
		name_tv.setText(sp[0]);
		
		for (int i=0;i<sp.length-1;i++) {
			String[] sp2 = sp[i+1].split(" ");
			String sname = sp2[0];
			String value = sp2[1];
			
			TableRow row = new TableRow(this);		
			TextView txt1 = new TextView(this.getApplicationContext());
			TextView txt2 = new TextView(this.getApplicationContext());		
			txt1.setText(sname);
			txt2.setText("  "+value);
			row.addView(txt1);
			row.addView(txt2);
			tl.addView(row);				
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
